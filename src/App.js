import "./App.css";
import { CatalogueProvider } from "./components/providers/catalogue";
import { CartProvider } from "./components/providers/cart";
import ProductList from "./components/product-list";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CatalogueProvider>
          <CartProvider>
            <ProductList type="catalogue" />
            <ProductList type="cart" />
          </CartProvider>
        </CatalogueProvider>
      </header>
    </div>
  );
}

export default App;
